# Breadcrumbs 面包屑

```{eval-rst}
.. inheritance-diagram:: nicegui_add.aui.breadcrumbs.Breadcrumbs

.. autoclass :: nicegui_add.aui.breadcrumbs.Breadcrumbs
   :members:
```

```{eval-rst}
.. inheritance-diagram:: nicegui_add.aui.breadcrumbs.BreadcrumbsEl

.. autoclass :: nicegui_add.aui.breadcrumbs.BreadcrumbsEl
   :members:
```