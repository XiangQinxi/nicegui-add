# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'NiceGUI-Add Docs'
copyright = '2023, XiangQinxi'
author = 'XiangQinxi'
release = '0.0.2'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.autodoc",  # 用于快速构建api文档
    "sphinx.ext.inheritance_diagram",  # 用于构建api文档继承关系
    "myst_parser",  # 用于使用更加简单的Markdown编写
    "sphinx_design"
]

myst_enable_extensions = ["colon_fence"]

html_theme_options = {
    "source_repository": "https://gitlab.com/XiangQinxi/nicegui-add",
    "source_branch": "master",
    "source_directory": "docs/",
}

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

language = 'zh_CN'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'furo'
html_static_path = ['_static']
